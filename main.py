from datetime import datetime

class Section():
    # All times are in seconds
    laps:list[int] = []
    timeTaken:int = 0
    successes:int = 0
    failures:int = 0
    successRate:float = 0
    
    def __init__(self, name:str) -> None:
        self.name:str = name
    
    def calculateSuccessRate(self):
        self.successRate = self.successes/(self.successes+self.failures)
    
    def addSuccesses(self, additionalSuccesses:int) -> None:
        self.successes += additionalSuccesses
        self.calculateSuccessRate()
        
    def addFailures(self, additionalFailures:int) -> None:
        self.failures += additionalFailures
        self.calculateSuccessRate()
    
    def addTime(self, timeTaken:int) -> None:
        self.laps.append(timeTaken)
        self.timeTaken = sum(self.laps)/len(self.laps)
        
Ramp = Section('Ramp')

input('Start Timer (enter)')

while True:
    one = datetime.now()
    result:str = input('Fail(n) or Success(Y)').upper()
    two = datetime.now()
    
    time = (two - one).seconds
    print(time)
    
    if result == 'N':
        Ramp.addFailures(1)
        Ramp.addTime(time)
        input('Start Timer (enter)')
    else:
        Ramp.addSuccesses(1)
        Ramp.addTime(time)
    
    print(Ramp.timeTaken)
    print(Ramp.successRate)